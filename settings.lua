function ModSettingsGuiCount() return 1 end
dofile_once("data/scripts/lib/utilities.lua")

local enemies_share_your_perks_settings = {}

function ModSettingsUpdate(init_scope)
  local curse_zones = {
    id = "CURSE_ZONES",
    type = "curse_zones",
    name = "Curse zones",
    key = "enemies_share_your_perks.curse_zones"
  }
  table.insert(enemies_share_your_perks_settings, curse_zones)
  if (ModSettingGet(curse_zones.key) == nil) then
    ModSettingSet(curse_zones.key, true)
  end

  local only_spawn_enemy_perks = {
    id = "ONLY_SPAWN_ENEMY_PERKS",
    type = "only_spawn_enemy_perks",
    name = "Only spawn enemy-usable perks",
    key = "enemies_share_your_perks.only_spawn_enemy_perks"
  }
  table.insert(enemies_share_your_perks_settings, only_spawn_enemy_perks)
  if (ModSettingGet(only_spawn_enemy_perks.key) == nil) then
    ModSettingSet(only_spawn_enemy_perks.key, false)
  end
end

function ModSettingsGui(gui, in_main_menu)
  local _id = 0
  local function id()
    _id = _id + 1
    return _id
  end

  GuiOptionsAdd(gui, GUI_OPTION.DrawActiveWidgetCursorOnBothSides)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "If on, zones along the main game path after the first holy mountain will be cursed")
  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "until the player collects the expected number of perks.")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  GuiText(gui, 0, 0, "   Curse zones:   ")
  local curse_zones = ModSettingGet("enemies_share_your_perks.curse_zones")
  if (curse_zones == nil) then
    curse_zones = true
  end
  local text = curse_zones and GameTextGet("$option_on") or GameTextGet("$option_off")
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("enemies_share_your_perks.curse_zones", not curse_zones)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "If on, only the 44 enemy-usable perks will be in the perk pool.")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  GuiText(gui, 0, 0, "   Only spawn enemy-usable perks:   ")
  local only_spawn_enemy_perks = ModSettingGet("enemies_share_your_perks.only_spawn_enemy_perks")
  if (only_spawn_enemy_perks == nil) then
    only_spawn_enemy_perks = false
  end
  local text = only_spawn_enemy_perks and GameTextGet("$option_on") or GameTextGet("$option_off")
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("enemies_share_your_perks.only_spawn_enemy_perks", not only_spawn_enemy_perks)
  end
  GuiLayoutEnd(gui)
end
