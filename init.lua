ModLuaFileAppend("data/scripts/perks/perk.lua", "mods/enemies_share_your_perks/scripts/perk_appends.lua")
ModLuaFileAppend("data/scripts/perks/perk_list.lua", "mods/enemies_share_your_perks/scripts/perk_list_appends.lua")

dofile_once("data/scripts/game_helpers.lua")
dofile_once("data/scripts/lib/utilities.lua")
dofile_once("data/scripts/perks/perk.lua")

dofile_once("mods/enemies_share_your_perks/scripts/lib.lua")

function OnWorldPreUpdate()
    local player = EntityGetWithTag("player_unit")[1]
	if player ~= nil then
        --make sure enemies get player perks
        enforce_perks(player, 512)
	end

    local curse_zones_enabled = ModSettingGet("enemies_share_your_perks.curse_zones")
    if curse_zones_enabled == nil or curse_zones_enabled == true then
        manage_curse_zones(player)
    end
end

function enforce_perks(player, radius)
    local x, y = EntityGetTransform(player)
    local enemy_ids = EntityGetInRadiusWithTag(x, y, 512, "enemy")
    for i = 1, #enemy_ids, 1 do
        local enemy_id = enemy_ids[i]
        if EntityHasTag(enemy_id, "enemies_share_your_perks") == false then
            --add tag immidiately regardless of if entity is modified 
            --so that we don't ever waste time checking it again
            EntityAddTag(enemy_id, "enemies_share_your_perks")
            if is_valid_enemy_for_perks(enemy_id) then
                give_player_perks_to_enemy(enemy_id)
            end
        end
    end
end

function is_valid_enemy_for_perks(enemy_id)
    if ( enemy_id == nil ) and ( enemy_id == NULL_ENTITY ) then
        return false
    end

    local enemy_filename = EntityGetFilename(enemy_id)
	local animals_path = "data/entities/animals/"
    local boss_path = "/boss_"
    local is_boss = string.find(enemy_filename, boss_path)
	if (string.sub(enemy_filename, 1, #animals_path) ~= animals_path)
    or (is_boss ~= nil) then
		return false
	end

    local worm = EntityGetComponent(target, "WormAIComponent")
    local dragon = EntityGetComponent(target, "BossDragonComponent")
    local ghost = EntityGetComponent(target, "GhostComponent")
    local lukki = EntityGetComponent(target, "LimbBossComponent")
	if (worm ~= nil) or (dragon ~= nil) or (ghost ~= nil) or (lukki ~= nil) then
        return false
    end
	
	return true
end

function give_player_perks_to_enemy(enemy_id)
    for _,perk_data in ipairs(perk_list) do
        if perk_data.usable_by_enemies ~= nil and perk_data.usable_by_enemies then
            local flag_name = get_perk_picked_flag_name(perk_data.id)
            local count = tonumber(GlobalsGetValue( flag_name .. "_PICKUP_COUNT", "0" ))
            if count ~= 0 then
                for i = 1, count do
                    --apply game effect, if any
                    if perk_data.game_effect ~= nil then
                        local game_effect_comp = GetGameEffectLoadTo(enemy_id, perk_data.game_effect, true)
                        if game_effect_comp ~= nil then
                            ComponentSetValue(game_effect_comp, "frames", "-1")
                        end
                    end
                    --apply perk function, if any
                    if perk_data.func_enemy ~= nil then
                        perk_data.func_enemy(0, enemy_id)
                    elseif perk_data.func ~= nil then
                        perk_data.func(0, enemy_id)
                    end
                    --apply particle effect, if any
                    if i == 1 then
                        if perk_data.particle_effect ~= nil then
                            local particle_id = EntityLoad("data/entities/particles/perks/" .. perk_data.particle_effect .. ".xml")
                            EntityAddTag(particle_id, "perk_entity")
                            EntityAddChild(enemy_id, particle_id)
                        end
                    end
                end
            end
        end
    end
end

function manage_curse_zones(player)
    local perk_count = get_perk_count()

    --only execute once every 30 frames
    local last_curse_zone_load = tonumber(GlobalsGetValue("enemies_share_your_perks.last_curse_zone_load", "30"))
    local this_frame_number = GameGetFrameNum()
    local frames_since_last_curse_zone_load = this_frame_number - last_curse_zone_load
    if frames_since_last_curse_zone_load >= 30 then
        GlobalsSetValue("enemies_share_your_perks.last_curse_zone_load", tostring(this_frame_number))
    else
        return
    end

    --continually fill in a 256-pixel curse zones grid defined by current location, perk count, and curse_zone areas
    local player_x, player_y = EntityGetTransform(player)
    local point_grid = quantized_point_grid_from_point(player_x, player_y, 256, 1)
    for i,point in ipairs(point_grid) do
        for j = perk_count + 1, 7 do
            if curse_zones[j] ~= nil then
                local curse_zone = curse_zones[j].zone
                local min_x = curse_zone.min_x * 512
                local max_x = curse_zone.max_x * 512
                local min_y = curse_zone.min_y * 512
                local max_y = curse_zone.max_y * 512
                if coordinates_are_in_box(point.x, point.y, min_x, min_y, max_x, max_y) then
                    local tag = "256_curse_zone_" .. tostring(point.x) .. "," .. tostring(point.y)
                    local existing_curse_zone = EntityGetInRadiusWithTag(point.x, point.y, 365, tag)[1]
                    if existing_curse_zone == nil then
                        --tag to claim the grid area as occupied
                        local new_curse_zone = EntityLoad("data/entities/misc/256_areadamage.xml", point.x, point.y)
                        EntityAddTag(new_curse_zone, tag)
                        --tag to indicate perks required to dispel the curse here
                        local perk_requirements_tag = "perks_required_" .. tostring(j)
                        EntityAddTag(new_curse_zone, perk_requirements_tag)
                    end
                end

                local holy_mountain = curse_zones[j].holy_mountain
                local x = holy_mountain.x * 512
                local y = holy_mountain.y * 512
                --check the SE quadrant of the holy mountain exit tile, specifically
                if coordinates_are_in_box(point.x, point.y, x, y + 256, x + 256, y + 512) then
                    local tag = "holy_mountain_curse_zone_" .. tostring(point.x) .. "," .. tostring(point.y)
                    local existing_curse_zone = EntityGetInRadiusWithTag(point.x, point.y, 365, tag)[1]
                    if existing_curse_zone == nil then
                        local new_curse_zone = nil
                        if j < 7 then
                            new_curse_zone = EntityLoad("data/entities/misc/holy_mountain_exit_areadamage.xml", point.x, point.y)
                        else
                            --different dimensions on the exit of the final holy mountain
                            new_curse_zone = EntityLoad("data/entities/misc/holy_mountain_kolmi_exit_areadamage.xml", point.x, point.y)
                        end
                        --tag to claim the grid area as occupied
                        EntityAddTag(new_curse_zone, tag)
                        --tag to indicate perks required to dispel the curse here
                        local perk_requirements_tag = "perks_required_" .. tostring(j)
                        EntityAddTag(new_curse_zone, perk_requirements_tag)
                    end
                end
            end
        end
    end
end