perk_blacklist = { 3,4,5,6,7,8,9,11,12,14,17,18,19,20,
22,25,27,28,29,30,31,32,33,40,42,43,44,45,46,49,59,63,
64,65,66,67,69,70,72,73,77,78,79,81,84,87,88,89,90,92,
93,95,96,97,98,99,100,101,102,103,104,105,106 }

-- this spawns perks in the temple
function perk_spawn_many( x, y, dont_remove_others_, ignore_these_ )
	local perk_count = tonumber( GlobalsGetValue( "TEMPLE_PERK_COUNT", "3" ) )
	
	local count = perk_count
	local width = 60
	local item_width = width / count
	local dont_remove_others = dont_remove_others_ or false
	local ignore_these = ignore_these_ or {}

	--MOD CHANGE HERE: inject list of non-enemy compatible perks to disable
	--if setting to disable them is set
	local only_spawn_enemy_perks = ModSettingGet("enemies_share_your_perks.only_spawn_enemy_perks")
	if only_spawn_enemy_perks == true then
		for i = 1, #perk_blacklist do
			table.insert(ignore_these, perk_list[perk_blacklist[i]].id)
		end
	end

	local perks = perk_get_spawn_order( ignore_these )

	for i=1,count do
		local next_perk_index = tonumber( GlobalsGetValue( "TEMPLE_NEXT_PERK_INDEX", "1" ) )
		local perk_id = perks[next_perk_index]
		
		while( perk_id == nil or perk_id == "" ) do
			-- if we over flow
			perks[next_perk_index] = "LEGGY_FEET"
			next_perk_index = next_perk_index + 1		
			if next_perk_index > #perks then
				next_perk_index = 1
			end
			perk_id = perks[next_perk_index]
		end

		next_perk_index = next_perk_index + 1
		if next_perk_index > #perks then
			next_perk_index = 1
		end

		GlobalsSetValue( "TEMPLE_NEXT_PERK_INDEX", tostring(next_perk_index) )
		GameAddFlagRun("PERK_" .. perk_id)
		perk_spawn( x + (i-0.5)*item_width, y, perk_id, dont_remove_others )
	end
end

-- first remove all the perks that are visible and count how many and where they are
function perk_reroll_perks( entity_item )

	local perk_spawn_pos = {}
	local perk_count = 0

	-- remove all perk items (also this one!) ----------------------------
	local all_perks = EntityGetWithTag( "perk" )
	local x, y
	if ( #all_perks > 0 ) then
		for i,entity_perk in ipairs(all_perks) do
			if entity_perk ~= nil then
				perk_count = perk_count + 1
				x, y = EntityGetTransform( entity_perk )
				table.insert( perk_spawn_pos, { x, y } )

				EntityKill( entity_perk )
			end
		end
	end

	local perk_reroll_count = tonumber( GlobalsGetValue( "TEMPLE_PERK_REROLL_COUNT", "0" ) )
	perk_reroll_count = perk_reroll_count + 1
	GlobalsSetValue( "TEMPLE_PERK_REROLL_COUNT", tostring( perk_reroll_count ) )

	local count = perk_count
	local width = 60
	local item_width = width / count

	--MOD CHANGE HERE: inject list of non-enemy compatible perks to disable
	--if setting to disable them is set
	local ignore_these = {}
	local only_spawn_enemy_perks = ModSettingGet("enemies_share_your_perks.only_spawn_enemy_perks")
	if only_spawn_enemy_perks == true then
		for i = 1, #perk_blacklist do
			table.insert(ignore_these, perk_list[perk_blacklist[i]].id)
		end
	end

	local perks = perk_get_spawn_order(ignore_these)

	for i,v in ipairs(perk_spawn_pos) do
		x = v[1]
		y = v[2]

		local next_perk_index = tonumber( GlobalsGetValue( "TEMPLE_REROLL_PERK_INDEX", tostring(#perks) ) )
		local perk_id = perks[next_perk_index]
		
		while( perk_id == nil or perk_id == "" ) do
			-- if we over flow
			perks[next_perk_index] = "LEGGY_FEET"
			next_perk_index = next_perk_index - 1		
			if next_perk_index <= 0 then
				next_perk_index = #perks
			end
			perk_id = perks[next_perk_index]
		end

		next_perk_index = next_perk_index - 1
		if next_perk_index <= 0 then
			next_perk_index = #perks
		end
		
		GlobalsSetValue( "TEMPLE_REROLL_PERK_INDEX", tostring(next_perk_index) )

		GameAddFlagRun("PERK_" .. perk_id)
		perk_spawn( x, y, perk_id )
	end
end

function perk_pickup( entity_item, entity_who_picked, item_name, do_cosmetic_fx, kill_other_perks, no_perk_entity_ )
	-- fetch perk info ---------------------------------------------------
	
	local no_perk_entity = no_perk_entity_ or false
	local pos_x, pos_y

	if no_perk_entity then
		pos_x, pos_y = EntityGetTransform( entity_who_picked )
	else
		pos_x, pos_y = EntityGetTransform( entity_item )
	end
	
	local perk_name = "PERK_NAME_NOT_DEFINED"
	local perk_desc = "PERK_DESCRIPTION_NOT_DEFINED"

	local perk_id = ""
	
	if no_perk_entity then
		perk_id = item_name
	else
		edit_component( entity_item, "VariableStorageComponent", function(comp,vars)
			perk_id = ComponentGetValue( comp, "value_string" )
		end)
	end

	local perk_data = get_perk_with_id( perk_list, perk_id )
	if perk_data == nil then
		return
	end
	
	-- Get perk's flag name
	
	local flag_name = get_perk_picked_flag_name( perk_id )
	
	-- update how many times the perk has been picked up this run -----------------
	
	local pickup_count = tonumber( GlobalsGetValue( flag_name .. "_PICKUP_COUNT", "0" ) )
	pickup_count = pickup_count + 1
	GlobalsSetValue( flag_name .. "_PICKUP_COUNT", tostring( pickup_count ) )

	-- load perk for entity_who_picked -----------------------------------
	local add_progress_flags = not GameHasFlagRun( "no_progress_flags_perk" )
	
	if add_progress_flags then
		local flag_name_persistent = string.lower( flag_name )
		if ( not HasFlagPersistent( flag_name_persistent ) ) then
			GameAddFlagRun( "new_" .. flag_name_persistent )
		end
		AddFlagPersistent( flag_name_persistent )
	end
	GameAddFlagRun( flag_name )
	
	local no_remove = perk_data.do_not_remove or false

	-- add a game effect or two
	if perk_data.game_effect ~= nil then
		local game_effect_comp,game_effect_entity = GetGameEffectLoadTo( entity_who_picked, perk_data.game_effect, true )
		if game_effect_comp ~= nil then
			ComponentSetValue( game_effect_comp, "frames", "-1" )
			
			if ( no_remove == false ) then
				ComponentAddTag( game_effect_comp, "perk_component" )
				EntityAddTag( game_effect_entity, "perk_entity" )
			end
		end
	end
	
	if perk_data.game_effect2 ~= nil then
		local game_effect_comp,game_effect_entity = GetGameEffectLoadTo( entity_who_picked, perk_data.game_effect2, true )
		if game_effect_comp ~= nil then
			ComponentSetValue( game_effect_comp, "frames", "-1" )
			
			if ( no_remove == false ) then
				ComponentAddTag( game_effect_comp, "perk_component" )
				EntityAddTag( game_effect_entity, "perk_entity" )
			end
		end
	end
	
	-- particle effect only applied once
	if perk_data.particle_effect ~= nil and ( pickup_count <= 1 ) then
		local particle_id = EntityLoad( "data/entities/particles/perks/" .. perk_data.particle_effect .. ".xml" )
		
		if ( no_remove == false ) then
			EntityAddTag( particle_id, "perk_entity" )
		end
		
		EntityAddChild( entity_who_picked, particle_id )
	end
	
	-- certain other perks may be marked as picked-up
	if perk_data.remove_other_perks ~= nil then
		for i,v in ipairs( perk_data.remove_other_perks ) do
			local f = get_perk_picked_flag_name( v )
			GameAddFlagRun( f )
		end
	end

	if perk_data.func ~= nil then
		perk_data.func( entity_item, entity_who_picked, item_name, pickup_count )
	end
	
	perk_name = GameTextGetTranslatedOrNot( perk_data.ui_name )
	perk_desc = GameTextGetTranslatedOrNot( perk_data.ui_description )

	-- add ui icon etc
	local entity_ui = EntityCreateNew( "" )
	EntityAddComponent( entity_ui, "UIIconComponent", 
	{ 
		name = perk_data.ui_name,
		description = perk_data.ui_description,
		icon_sprite_file = perk_data.ui_icon
	})
	
	if ( no_remove == false ) then
		EntityAddTag( entity_ui, "perk_entity" )
	end
	
	EntityAddChild( entity_who_picked, entity_ui )

	-- cosmetic fx -------------------------------------------------------
	if do_cosmetic_fx then
		local enemies_killed = tonumber( StatsBiomeGetValue("enemies_killed") )
		
		if( enemies_killed ~= 0 ) then
			EntityLoad( "data/entities/particles/image_emitters/perk_effect.xml", pos_x, pos_y )
		else
			EntityLoad( "data/entities/particles/image_emitters/perk_effect_pacifist.xml", pos_x, pos_y )
		end
		
		GamePrintImportant( GameTextGet( "$log_pickedup_perk", GameTextGetTranslatedOrNot( perk_name ) ), perk_desc )
	end
	
	-- disable the perk rerolling machine --------------------------------
	local x,y = EntityGetTransform( entity_who_picked )
	local rerolls = EntityGetInRadiusWithTag( x, y, 200, "perk_reroll_machine" )
	local other_perks = EntityGetInRadiusWithTag( x, y, 200, "item_perk" )
	
	print( "Other perks: " .. tostring( #other_perks ) .. ", " .. tostring( kill_other_perks ) )
	
	local disable_reroll = false
	
	if ( #other_perks <= 1 ) then
		disable_reroll = true
	end
	
	-- remove all perk items (also this one!) ----------------------------
	if kill_other_perks then
		local perk_destroy_chance = tonumber( GlobalsGetValue( "TEMPLE_PERK_DESTROY_CHANCE", "100" ) )
		SetRandomSeed( pos_x, pos_y )

		if( Random( 1, 100 ) <= perk_destroy_chance ) then
			-- removes all the perks
			local all_perks = EntityGetWithTag( "perk" )
			disable_reroll = true
		
			if ( #all_perks > 0 ) then
				for i,entity_perk in ipairs(all_perks) do
					if entity_perk ~= entity_item then
						EntityKill( entity_perk )
					end
				end
			end
		end
	end
	
	if disable_reroll then
		for i,rid in ipairs( rerolls ) do
			local reroll_comp = EntityGetFirstComponent( rid, "ItemCostComponent" )
			
			if ( reroll_comp ~= nil ) then
				EntitySetComponentIsEnabled( rid, reroll_comp, false )
			end
			
			reroll_comp = EntityGetComponent( rid, "SpriteComponent", "shop_cost" )
			
			if ( reroll_comp ~= nil ) then
				for a,b in ipairs( reroll_comp ) do
					EntitySetComponentIsEnabled( rid, b, false )
				end
			end
			
			EntitySetComponentsWithTagEnabled( rid, "perk_reroll_disable", false )
		end
	end
	
	if ( no_perk_entity == false ) then
		EntityKill( entity_item ) -- entity item should always be killed, hence we don't kill it in the above loop
	end

	--MOD CHANGE HERE: straightforward tracking of perk pickup count with a global value,
	--vanilla game doesn't seem to do this as it's never needed there
    local perk_count = tonumber(GlobalsGetValue("enemies_share_your_perks.perk_count", "0"))
    GlobalsSetValue("enemies_share_your_perks.perk_count", tostring(perk_count + 1))
end