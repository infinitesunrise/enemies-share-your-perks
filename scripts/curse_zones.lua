--build table of curse zones.
--indexes correspond to the number of
--perks required to uncurse those zones

--world origin on biome_map.png is pixel 35,14

curse_zones = {}

table.insert(curse_zones, 1, {
    name = "coal_mines",
    holy_mountain = {
        x = 0,
        y = 2
    },
    zone = {
        min_x = -4,
        max_x = 4,
        min_y = 3,
        max_y = 6
    }
})

table.insert(curse_zones, 2, {
    name = "snowy_depths",
    holy_mountain = {
        x = 0,
        y = 5
    },
    zone = {
        min_x = -5,
        max_x = 5,
        min_y = 6,
        max_y = 10
    }
})

table.insert(curse_zones, 3, {
    name = "hiisi_base",
    holy_mountain = {
        x = 0,
        y = 9
    },
    zone = {
        min_x = -4,
        max_x = 3,
        min_y = 10,
        max_y = 13
    }
})

table.insert(curse_zones, 4, {
    name = "underground_jungle",
    holy_mountain = {
        x = 0,
        y = 12
    },
    zone = {
        min_x = -5,
        max_x = 4,
        min_y = 13,
        max_y = 17
    }
})

table.insert(curse_zones, 5, {
    name = "the_vault",
    holy_mountain = {
        x = 0,
        y = 16
    },
    zone = {
        min_x = -6,
        max_x = 5,
        min_y = 17,
        max_y = 21
    }
})

table.insert(curse_zones, 6, {
    name = "temple_of_the_art",
    holy_mountain = {
        x = 0,
        y = 20
    },
    zone = {
        min_x = -6,
        max_x = 5,
        min_y = 21,
        max_y = 26
    }
})

table.insert(curse_zones, 7, {
    name = "kolmisilma",
    holy_mountain = {
        x = 5,
        y = 25
    },
    zone = {
        min_x = 6,
        max_x = 8,
        min_y = 25,
        max_y = 26
    }
})