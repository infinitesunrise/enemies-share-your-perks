--enemy_func for projectile repulsion field
local new_repulsion_sector_func_enemy = function(entity_perk_item, entity_who_picked)
    --get projectile_repulsion if already exists
    local projectile_repulsion = nil
    local children = EntityGetAllChildren(entity_who_picked)
    if children ~= nil then
        for _,child in ipairs(children) do
            local name = EntityGetName(child)
            if name == "projectile_repulsion" then
                projectile_repulsion = child
            end
        end
    end

    --handle perk count
    if projectile_repulsion ~= nil then
        local variable_storage = get_variable_storage_component(projectile_repulsion, "perk_pickup_count")
        if variable_storage ~= nil then
            local count = nil
            component_read(variable_storage, { value_int = 0 }, function(comp)
                count = comp.value_int
            end)
            ComponentSetValue2(variable_storage, "value_int", count + 1)
        end
    else
        local x,y = EntityGetTransform(entity_who_picked)
        local repulsion_field_id = EntityLoad("data/entities/misc/perks/projectile_repulsion_field.xml", x, y)
        EntityAddTag(repulsion_field_id, "perk_entity")
        EntityAddComponent(repulsion_field_id, "VariableStorageComponent", 
        { 
            name = "perk_pickup_count",
            value_int = 1,
        })
        EntityAddChild(entity_who_picked, repulsion_field_id)
    end

    --adjust projectile damage
    local damagemodels = EntityGetComponent( entity_who_picked, "DamageModelComponent" )
    if( damagemodels ~= nil ) then
        for i,damagemodel in ipairs(damagemodels) do
            local projectile_resistance = tonumber(ComponentObjectGetValue( damagemodel, "damage_multipliers", "projectile" ))
            projectile_resistance = projectile_resistance * 1.26
            ComponentObjectSetValue( damagemodel, "damage_multipliers", "projectile", tostring(projectile_resistance) )
        end
    end
end
perk_list[68].func_enemy = new_repulsion_sector_func_enemy

--enemy_func for angry ghost
local new_angry_ghost_func_enemy = function( entity_perk_item, entity_who_picked, item_name )
    local x,y = EntityGetTransform( entity_who_picked )
    local child_id = EntityLoad( "data/entities/misc/perks/angry_ghost.xml", x, y )
    EntityAddTag( child_id, "perk_entity" )
    EntityAddChild( entity_who_picked, child_id )
    
    EntityAddComponent( entity_who_picked, "LuaComponent", 
    {
        _tags = "perk_component",
        script_shot = "data/scripts/perks/angry_ghost_shoot_enemy.lua",
        execute_every_n_frame = "1",
    } )
end
perk_list[75].func_enemy = new_angry_ghost_func_enemy

--helper functions for repulsion field's perk count tracking
function get_variable_storage_component(entity_id, name)
	local components = EntityGetComponent(entity_id, "VariableStorageComponent")
	if(components ~= nil) then
		for _,comp_id in pairs(components) do 
			local var_name = ComponentGetValue(comp_id, "name")
			if(var_name == name) then
				return comp_id
			end
		end
	end
	return nil
end
function set_variable_storage_component(entity_id, name, value)
    local variable_storage = get_variable_storage_component(entity_id, name)
    if variable_storage ~= nil then
        ComponentSetValue2(variable_storage, name, value)
    else
        EntityAddComponent(entity_id, "VariableStorageComponent", 
        { 
            name = name,
            value = value,
        })
    end
end