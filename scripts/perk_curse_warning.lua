--get this curse zone
local comp_id = GetUpdatedEntityID()
local entity_id = EntityGetRootEntity(comp_id)

--get this zone's area damage component
local area_damage_component = nil
local area_damage_components = EntityGetComponent(entity_id, "AreaDamageComponent")
if area_damage_components ~= nil then
    area_damage_component = area_damage_components[1]
end

--player entity and frame-based throttling
local player = EntityGetWithTag("player_unit")[1]
local last_message_frame = tonumber(GlobalsGetValue("enemies_share_your_perks.perk_curse_last_message", "0"))
local this_frame_number = GameGetFrameNum()
local frames_since_last_message = this_frame_number - last_message_frame

if player ~= nil 
and area_damage_component ~= nil
and frames_since_last_message >= 200 then
    local entity_x, entity_y = EntityGetTransform(entity_id)
    local perk_count = tonumber(GlobalsGetValue("enemies_share_your_perks.perk_count", "0"))

    --perk requirement is stored as part of a tag string
    --because why not
    local perk_requirement = 0
    local tags = EntityGetTags(entity_id)
    for tag in string.gmatch(tags, "[^,]+") do
        local is_perk = string.find(tag, "perks_required_")
        if is_perk ~= nil then
            local _,requirement = string.match(tag, "(.*)%_(.*)")
            if requirement ~= nil then
                perk_requirement = tonumber(requirement)
            end
        end
    end

    local num_more_perks = perk_requirement - perk_count
    if num_more_perks > 0 then
        local player_x, player_y = EntityGetTransform(player)
        local aabb_min_x, aabb_min_y = ComponentGetValueVector2(area_damage_component, "aabb_min")
        local aabb_max_x, aabb_max_y = ComponentGetValueVector2(area_damage_component, "aabb_max")
        local box_min_x = entity_x + aabb_min_x
        local box_max_x = entity_x + aabb_max_x
        local box_min_y = entity_y + aabb_min_y
        local box_max_y = entity_y + aabb_max_y

        if box_min_x < player_x
        and player_x < box_max_x
        and box_min_y < player_y
        and player_y < box_max_y then
            local cheeky_increment = tonumber(GlobalsGetValue("enemies_share_your_perks.perk_curse_cheeky", "0"))
            local plural_s = ""
            if num_more_perks > 1 then plural_s = "s" end
            --standard warning
            local warning = "collect " .. tostring(num_more_perks) .. " more perk" .. tostring(plural_s) .. " to dispel this zone's curse"

            --reset cheeky counter if the player left for a bit
            if cheeky_increment > 0 and frames_since_last_message > 500 then
                cheeky_increment = 0
            end

            --cheeky warnings
            if cheeky_increment == 1 then
                warning = "seriously, you'll probably die if you stay here"
            end
            if cheeky_increment == 2 then
                warning = "this wasn't really supposed to be the challenge but ok"
            end

            --allow cheeky_increment to go to 3, to loop back to standard warning in perpetuity if player sits in cursed zone
            if cheeky_increment < 3 then
                cheeky_increment = cheeky_increment + 1
            end

            GamePrintImportant("YOU REQUIRE MORE PERKS", warning, "data/ui_gfx/decorations/3piece_curse_msg.png")
            GlobalsSetValue("enemies_share_your_perks.perk_curse_cheeky", tostring(cheeky_increment))
            GlobalsSetValue("enemies_share_your_perks.perk_curse_last_message", tostring(this_frame_number))
        end
    else
        --goodnight sweet prince
        EntityKill(entity_id)
    end
end