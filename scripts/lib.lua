dofile_once("data/scripts/perks/perk_list.lua")

dofile_once("mods/enemies_share_your_perks/scripts/curse_zones.lua")

function coordinates_are_in_box(x, y, bbx_min, bby_min, bbx_max, bby_max)
    if bbx_min < x
    and x < bbx_max
    and bby_min < y
    and y < bby_max then
        return true
    end
    return false
end

--returns an table of CENTER POINTS for a quantized grid around a given point
function quantized_point_grid_from_point(x, y, increment, grid_radius)
    local offset = increment / 2
    local center_box_x = math.floor(x / increment) * increment
    local center_box_y = math.floor(y / increment) * increment
    box_grid = {}
    for pointer_x = -grid_radius, grid_radius do
        for pointer_y = -grid_radius, grid_radius do
            table.insert(box_grid, {
                x = (pointer_x * increment) + center_box_x + offset,
                y = (pointer_y * increment) + center_box_y + offset
            })
        end
    end
    return box_grid
end

function get_perk_count()
    local perk_count = tonumber(GlobalsGetValue("enemies_share_your_perks.perk_count", "0"))
    return perk_count
end